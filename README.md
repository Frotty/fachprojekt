# Fachprojekt WS 18/19

Zugriffsdaten für Folien:
http://patrec.cs.tu-dortmund.de/cms/en/home/Teaching/WS18/FP_Dokumentenanalyse/

User: DA2018
Password: washington

Infos für Aufgabe 4
4 Nachbarn, relatives Term weighting, großes Vokabular ca 2000


Distanzfunktionen: City-Block geht am besten bei großen Merkmalsvektoren

Plots sind in der PCA-Example vorbereitet

Hinweise zu Aufgabe 6:


Bag-Of-Words bei Bilddaten

Am Ende von Aufg. 6 Algorithmus implementieren um Feature Vektoren zu Clustern.

Schritt 1: Zentroiden wählen
Schritt 2: Alle Datenpunkte dem nächsten Zentroiden zuordnen
Schritt 3: Zentroiden "Updaten" Neuer Zentroid ist Mittelpunkt des in Schritt 2 entstandenen Clusters
Schritt 4: Wieder Alle Datenpunkte den neuen Zentroiden zuordnen
Schritte 3 und 4 wiederholen bis keine Verbesserung der Zentroiden mehr stattfindet. Dann ist die optimale Zuordnung gefunden
